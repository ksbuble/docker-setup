#!/bin/bash
sudo rm -rf data/*
sudo docker-compose -f docker-compose-backup.yml up
pushd data && sudo tar -zxvf backup.tgz && rm -f backup.tgz && popd
