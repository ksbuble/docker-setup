# Instructions 

The following are the instructions for using the setup files in this repository to
set up a Vagrant Virtual Box image and run Docker on that image.

## VirtualBox

This Vagrant development configuration requires [VirtualBox](https://www.virtualbox.org/) and [Vagrant](https://docs.vagrantup.com). Search for [VirtualBox](https://atlas.hashicorp.com/) images, or create a new base box from [scratch](http://aruizca.com/steps-to-create-a-vagrant-base-box-with-ubuntu-14-04-desktop-gui-and-virtualbox/). The current image used in this configuration is: "box-cutter/ubuntu1404-desktop"

## Vagrant
Once Vagrant is installed, create a new box:

1.  Go to the Vagrant folder in the terminal, create a new folder, cd into it
2.  Execute $vagrant init <box-name or box-url (ex. hashicorp/precise64)>
This creates a file named Vagrantfile, specifying the Linux box image to provision the virtual machine with.
3.  Provision the box with Vagrant: $vagrant up --provision or just $vagrant provision

If the box is already provisioned start the virtual box with Vagrant:

1.  Go to box location in Vagrant folder in terminal and execute $vagrant up. 
2.  SSH into the box with Vagrant: $vagrant ssh. The vagrant user can log in without a password.
3.  To stop the box: $vagrant halt

### SSH port forwarding

Expose the SSH port on the VirtualBox to enable SSH from the Host. 

1.  On the host machine, port 2222 is set by Vagrant by default to allow connection to the box via SSH. This is mapped to port 22 on the virtual machine. These settings enable command $vagrant ssh to connect to the box. 
2.  If need to connect to the box directly via the IP address (set in the Vagrantfile), for example $ssh vagrant@127.0.0.1 -p 2222, rather than using the $vagrant ssh command then on the host open file ~/.ssh/config and add:
Host vagrant
  HostName 127.0.0.1
  User vagrant
  Port 2222

For passwordless connections: 

1.  On the Host create an ssh key pair. Use ssh-keygen to generate an id.rsa file if one is not yet present.
2.  Add the public SSH key by copying the contents of  "~/.ssh/id_rsa.pub" host file to "~/.ssh/authorized_keys" on the VirtualBox. 


## Docker
On the VirtualBox using Vagrant, install [Docker for Linux](https://docs.docker.com/installation/ubuntulinux/) and [Docker compose](https://github.com/docker/compose/releases). The provisioning for these is specified in the VagrantFile.

### Containers
Containers used are:

1.  [Docker Tripal container](http://tripal.info/node/204) 
ex. https://github.com/erasche/docker-tripal/blob/master/Dockerfile
2. [Docker Drupal container](https://github.com/samos123/docker-drupal/blob/master/README.md) (without a database).

Other docker containers available at: https://hub.docker.com

### Provisioning

The Drupal container has database and web files that we wish to persist. If provisioning Docker for the first time, run the initalization script which will create persistent data volumes:

1. On the Virtual machine, run the initialization file from the commandline ~/docker-tripal-master/init.sh. This runs ~/docker-tripal-master/docker-compose-backup.yml which creates the backup image. The backup image's volume consists of the tripal web and chado db image volumes. The initialization file then creates a tarball from the backup image volume, extracts the data files from the tarball, copies them over to ~/docker-tripal-master/data and removes the tarball.

2. If starting Docker after already previously provisioned, skip the first step which to avoid losing the persisted data. Instead, clean up if Docker did not shut down gracefully last time it ran. If some containers were left running, first stop and delete the existing containers:

* $sudo docker-compose stop
* $sudo docker ps -a  # take the container ids and execute for each:
* $sudo docker rm -v ID

Then start the Docker Drupal container from the docker-tripal-master directory:
$sudo docker-compose up 
This runs the ~/docker-tripal-master/docker-compose.yml file which creates the web and db images. The content for the volumes in these images is supplied by the files persisted at ~/docker-tripal-master/data, so each time the container is started, these same persisted files are used.

