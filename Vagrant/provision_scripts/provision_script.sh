#!/usr/bin/env bash

apt-get update 

#apt-get upgrade  
# work around error: "The GRUB boot loader was previously installed to a disk..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade
apt-get install build-essential -y

#install git
apt-get -y install git 

#install sshfs to map to a remote file system (mainlab.bioinfo...)
apt-get install sshfs

#install vim
apt-get install vim

#install drush
apt-get install drush

#install wget
DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends install wget

apt-get -y install docker.io
pip install docker-compose==1.3

# apt-get install docker-compose
# pip install -U docker-compose
# apt-get -y install python.pip
# apt-get install python-pip
     
